/*  s24-discussion.js  */

// Comparison Query Operators
// $gt/$gte operator

	db.collectionName.find({ field}) ....
	db.collectionName.find .....

	db.users.find({ age: { $gt : 50 }})


// $lt/$lte operator
	db.users.find({ age : { $lt : 50 } }).pretty();
	db.users.find({ age : { $lte : 50 } }).pretty();

// $ne operator
	db.users.find({ age : { $ne : 82} }).pretty();

// $in operator
	db.users.find( { lastName: { $in: ["Hawking", "Doe" ] } } ).pretty();
	db.users.find( ....

// Logical Query Operators
// $or operator

	db.users.find( { $or: [ { firstName: "Neil" }, { age: "25" } ] } ).pretty()

// $and operator
	db.users.find({ $and: [ { age : { $ne: 82 } }, { age : { $ne:76} }] } ).pretty();

// Field Projection
// Retrieving documents are commom operations that we do and by
// default MongoDB queries return the whole document as a response

// When dealing with complex data structures, there might
// be instances when fields are not useful for the query that
// we are trying to accomplish

// To help with readability of the values returned,
// we can include/exclude fields fr the response 


// Inclusion

//	- allows us to include/add specific fields only when retrieving docs
//  - the value provided is 1 to denote that the field is being included.
//  - Syntax
		db.users.find({criteria},{field: 1})

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1
	}
)


db.users.find(
	{
		firstName: "Jane"
	},
	{
		contact: 0,
		department: 0
	}
).pretty();



// Suppresssing the ID field
/*

*/
db.users.find(
    {
		firstName: "Jane"
    },
    {
		firstName: 1,
		lastName: 1
		contact: 1,
		_id: 0
    }
).pretty();   // Error: Line 8: Unexpected identifier



// reference-based









// embedded approach
{
	_id: <ObjectId1>,
	username: "123xyz",
	contact: {
		phone:"",
		email:""
	},
	access: {
		level: 5,
		group: "dev"
	}
}


// =============================
	//embedded approach

	{
		id: 12345,
		title: "Titanic",
		genre: "Romance",
		reviews: [
			{
				name: "Jherson"
				rating: "5",
				comments: "Lorem Ipsum"
			},
			{
				name: "Ethan"
				rating: "5",
				comments: "Lorem Ipsum"
			},
		]
	}

// =============================
	//reference approach

{
		id: 12345,
		title: "Titanic",
		genre: "Romance",
		reviews: [
			{
				reviewer_id: 12458-3352
			},
			{
				reviewer_id: 155658-3352
			},
		]
	}


// =============================

reviews document


{
	name: "Hannah",
	courses: [
		{
				course_name: "HTML"
		},

		{
				course_name: "PHP"
		}

	]
}



// Evaluation Query Operators

	// $regex operator
	/*
		- Allows us to find docs that match a specific
			string pattern using reg expressions
		- Syntax
			db.users.find({ field: $regex: 'pattern', $options: '$optionValue' });
	*/

	// Case sensitive query
	db.users.find({ firstName: { $regex: 'N' } })

	// Case insensitive query
	db.users.find({ firstName: { $regex: 'j', $options: '$i' } }).pretty();

// =============================
{
name: "Hannah",
course: [
	{
		courseName: "HTML"
	},
	{
		courseName: "PHP"
	}
	]
}












